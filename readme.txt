The D3 example:
  - https://www.tutorialspoint.com/d3js/d3js_quick_guide.htm
The match exasmple:
  - http://mathjs.org/examples/index.html
The plotly example
 - https://plot.ly/javascript/line-charts/

A good youtube video for using IntelliJ to work with Javascript
 - https://www.youtube.com/watch?v=Z0dPtpsQYag&list=PLaYPRjQvN6ITml9m0RW0fZA6ZotCNmUsV

 jQuery example:
  - https://www.w3schools.com/jquery/

Software needed:
  - JDK 8
    https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
  - IntelliJ Ultimate Edition
    https://www.jetbrains.com/idea/download
  - Tomcat Server download
    https://www-us.apache.org/dist/tomcat/tomcat-8/v8.5.35/bin/apache-tomcat-8.5.35.zip


IntelliJ Shortcut
 - auto formatting: Cmd+Alt+L
 - commit: Cmd+k
 - push: Cmd+shift+k

To clone the example:
 - mkdir demo
 - cd demo
 - git clone https://jimsun@bitbucket.org/jimsun/jsdemo1.git .

Git commands:
 - git pull        # to get the latest code
 - git add <file>  # to stage a new file
 - git commit -m "message or comments"  #
 - git push